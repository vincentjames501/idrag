var viewController = new Array();


viewController["advertised_intake_closing"] = function() {
	computations = new Array();
	computations["advertised_intake_closing_point"] = function(dataValues) {
		var iVCP = parseFloat(dataValues["intake_valve_closing_point"]);
		var iD = parseFloat(dataValues["intake_duration"]);
		var iAD = parseFloat(dataValues["intake_advertised_duration"]);
		return iVCP-(iD-iAD)/2.0;
	};

	additionalCalculations = new Array();
	additionalCalculations.push(function(dataValues){
		var answer = $("input[name$='advertised_intake_closing_point']");
		answer.attr("value", "" + answer.attr("value")+" ABDC");
	});				
};


viewController["air_flow_requirements"] = function() {
	computations = new Array();
	computations["cfm"] = function(dataValues){
		var c_f_m = parseFloat(dataValues["cid"])*parseFloat(dataValues["rpm"])*(parseFloat(dataValues["volumetric_efficiency"])/100.0)/3456.0;				
		if($("#engine_type").attr("value")!="four_stroke") {
			c_f_m*=2.0;
		}
		return c_f_m;
	};
	computations["lbs_of_air"] = function(dataValues){
		var c_f_m = parseFloat(dataValues["cid"])*parseFloat(dataValues["rpm"])*(parseFloat(dataValues["volumetric_efficiency"])/100.0)/3456.0;
		var lbsA = (14.72*c_f_m*29)/(10.73*(parseFloat(dataValues["air_temperature"])+459.67));	
		if($("#engine_type").attr("value")!="four_stroke") {
			lbsA*=2.0;
		}
		return lbsA;
	};
	computations["lbs_of_fuel"] = function(dataValues){
		var c_f_m = parseFloat(dataValues["cid"])*parseFloat(dataValues["rpm"])*(parseFloat(dataValues["volumetric_efficiency"])/100.0)/3456.0;
		var lbsF = ((14.72*c_f_m*29)/(10.73*(parseFloat(dataValues["air_temperature"])+459.67)))/14.7499888;				
		if($("#engine_type").attr("value")!="four_stroke") {
			lbsF*=2.0;
		}
		return lbsF;
	};
}




viewController["brake_hp_loss"] = function() {
	computations = new Array();
	computations["bhp_loss"] = function(dataValues){
		return dataValues["elevation"]/1000*0.03*dataValues["bhp_sea_level"];
	};
}




viewController["brake_hp_torque_rpm"] = function() {
	computations = new Array();
	computations["hp"] = function(dataValues){return (dataValues["rpm"] * dataValues["torque"]) / 5252.0;};
	computations["torque"] = function(dataValues){return (5252.0 * dataValues["hp"]) / dataValues["rpm"];};
	computations["rpm"] = function(dataValues){return (5252.0 * dataValues["hp"]) / dataValues["torque"];};
}



viewController["bsfc"] = function() {
	computations = new Array();
	computations["bsfc"] = function(dataValues){	
		return parseFloat(dataValues["fuel_lbs"])/parseFloat(dataValues["bhp"])*100; 
	};
}


viewController["calculate_piston_volume"] = function() {
	computations = new Array();
	computations["volume_cc"] = function(dataValues) {
		return Math.abs(dataValues["measured_volume"]-(Math.PI/4.0*Math.pow(dataValues["bore"],2)*dataValues["piston_depth"])*16.387);
	};
	computations["volume_ci"] = function(dataValues) {
		return Math.abs((dataValues["measured_volume"]-(Math.PI/4.0*Math.pow(dataValues["bore"],2)*dataValues["piston_depth"])*16.387)/16.387);
	};
	additionalCalculations = new Array();
	additionalCalculations.push(function(dataValues){
		var vol = dataValues["measured_volume"]-(Math.PI/4.0*Math.pow(dataValues["bore"],2)*dataValues["piston_depth"])*16.387;
		if(vol>=0) {
			populateFieldWithValue("piston_type", "Dished");
		} else {
			populateFieldWithValue("piston_type", "Domed")
		}
	});
}

viewController["center_of_gravity"] = function() {
	computations = new Array();
	computations["horizontal_cg"] = function(dataValues){	
		var aa = parseFloat(dataValues["a"]);
		var bb = parseFloat(dataValues["b"]);
		var cc = parseFloat(dataValues["c"]);
		var dd = parseFloat(dataValues["d"]);
		var ee = parseFloat(dataValues["e"]);
		var ff = parseFloat(dataValues["f"]);
		return ((cc/100.0)*aa);
	};
	computations["height_cg"] = function(dataValues){
		var aa = parseFloat(dataValues["a"]);
		var bb = parseFloat(dataValues["b"]);
		var cc = parseFloat(dataValues["c"]);
		var dd = parseFloat(dataValues["d"]);
		var ee = parseFloat(dataValues["e"]);
		var ff = parseFloat(dataValues["f"]);
		return ((aa*dd*ff)/(ee*bb));
	};
}

viewController["compression_boost"] = function() {
	computations = new Array();
	computations["effective_compression_ratio"] = function(dataValues) {
		return (((dataValues["boost"]/14.7)+1)*dataValues["static_compression_ratio"]);
	};
}


viewController["convert_quarter_to_eighth"] = function() {
	computations = new Array();
	computations["quarter"] = function(dataValues){
		return dataValues["eighth"] * 1.5832;
	};
	computations["eighth"] = function(dataValues){
		return dataValues["quarter"] / 1.5832;
	};
}


viewController["correct_da_to_another"] = function() {
	computations = new Array();
	computations["corrected_end_time"] = function(dataValues) {
		var mDA = parseFloat(dataValues["measured_da"]);
		var cDA = parseFloat(dataValues["correct_da"]);
		var eT = parseFloat(dataValues["end_time"]);
		var tS = parseFloat(dataValues["trap_speed"]);
		var cET = 0.0;
		var cTS = 0.0;
		var dafET1 = 1.003-(0.0013*mDA/100.0); //density altitude factor et 1
		var dafET2 = 1.003-(0.0013*cDA/100.0); //density altitude factor et 2
		var dafTS1 = 0.9961+(0.0014*mDA/100.0); //denstiy altitude factor ts 1
		var dafTS2 = 0.9961+(0.0014*cDA/100.0); //denstiy altitude factor ts 2
		cET = eT/dafET2*dafET1;
		cTS = tS/dafTS2*dafTS1;
		return cET;
	};
	computations["corrected_trap_speed"] = function(dataValues) {
		var mDA = parseFloat(dataValues["measured_da"]);
		var cDA = parseFloat(dataValues["correct_da"]);
		var eT = parseFloat(dataValues["end_time"]);
		var tS = parseFloat(dataValues["trap_speed"]);
		var cET = 0.0;
		var cTS = 0.0;
		var dafET1 = 1.003-(0.0013*mDA/100.0); //density altitude factor et 1
		var dafET2 = 1.003-(0.0013*cDA/100.0); //density altitude factor et 2
		var dafTS1 = 0.9961+(0.0014*mDA/100.0); //denstiy altitude factor ts 1
		var dafTS2 = 0.9961+(0.0014*cDA/100.0); //denstiy altitude factor ts 2
		cET = eT/dafET2*dafET1;
		cTS = tS/dafTS2*dafTS1;
		return cTS;
	};
}



viewController["crank_hp_to_whp"] = function() {
	computations = new Array();
	computations["hp_at_wheels"] = function(dataValues){return dataValues["hp_at_engine"] * (1-dataValues["percent_loss"]*0.01);};
	computations["hp_at_engine"] = function(dataValues){return dataValues["hp_at_wheels"] / (1-dataValues["percent_loss"]*0.01);};
	computations["percent_loss"] = function(dataValues){return (1-(dataValues["hp_at_wheels"] / dataValues["hp_at_engine"]))*100;};
}


viewController["cross_sectional_area"] = function() {
	computations = new Array();
	computations["ca_required"] = function(dataValues){
		var b = parseFloat(dataValues["bore"]);
		var s = parseFloat(dataValues["stroke"]);
		var r = parseFloat(dataValues["rpm"]);
		var f = parseFloat(dataValues["feet_per_second"]);
		return b*b*s*r*0.00353/f;	
	};
}


viewController["dynamic_compression_ratio"] = function() {
	computations = new Array();
	computations["dynamic_compression_ratio"] = function(dataValues) {
		var cHV = parseFloat(dataValues["cylinder_head_volume"]);
		var pHV = parseFloat(dataValues["piston_head_volume"]);
		var hGT = parseFloat(dataValues["head_gasket_thickness"]);
		var hGB = parseFloat(dataValues["head_gasket_bore"]);
		var b = parseFloat(dataValues["bore"]);
		var dC = parseFloat(dataValues["deck_clearance"]);
		var s = parseFloat(dataValues["stroke"]);
		var rL = parseFloat(dataValues["rod_length"]);
		var iCP = parseFloat(dataValues["intake_closing_poing"]);
		var dCR = 0.0;
		var DST = 0.0;
		var RD = .5*s*Math.sin(iCP*3.14159265358979323846/180.0); 
		var RR = .5*s*Math.cos(iCP*3.14159265358979323846/180.0); 
		var PR1 = Math.sqrt((rL*rL) - (RD*RD)) ;
		var PR2 = PR1 - RR ;
		DST = s - ((PR2 + (.5*s)) - rL) ;
		var GV = hGB*hGB*12.87*hGT;
		var DV = b*b*12.87*dC;
		var PV = b*b*DST*12.87;
		dCR = (GV+DV+cHV-pHV+PV)/(GV+DV+cHV-pHV);
		return dCR;
	};
	computations["dynamic_stroke"] = function(dataValues) {
		var cHV = parseFloat(dataValues["cylinder_head_volume"]);
		var pHV = parseFloat(dataValues["piston_head_volume"]);
		var hGT = parseFloat(dataValues["head_gasket_thickness"]);
		var hGB = parseFloat(dataValues["head_gasket_bore"]);
		var b = parseFloat(dataValues["bore"]);
		var dC = parseFloat(dataValues["deck_clearance"]);
		var s = parseFloat(dataValues["stroke"]);
		var rL = parseFloat(dataValues["rod_length"]);
		var iCP = parseFloat(dataValues["intake_closing_poing"]);
		var dCR = 0.0;
		var DST = 0.0;
		var RD = .5*s*Math.sin(iCP*3.14159265358979323846/180.0); 
		var RR = .5*s*Math.cos(iCP*3.14159265358979323846/180.0); 
		var PR1 = Math.sqrt((rL*rL) - (RD*RD)) ;
		var PR2 = PR1 - RR ;
		DST = s - ((PR2 + (.5*s)) - rL) ;
		var GV = hGB*hGB*12.87*hGT;
		var DV = b*b*12.87*dC;
		var PV = b*b*DST*12.87;
		dCR = (GV+DV+cHV-pHV+PV)/(GV+DV+cHV-pHV);
		return DST;
	};
}



viewController["engine_calculations"] = function() {
	computations = new Array();
	computations["stroke"] = function(dataValues){return dataValues["displacement"]/(Math.pow(dataValues["bore"]/2,2)*Math.PI*dataValues["number_of_cylinders"]);};
	computations["bore"] = function(dataValues){return Math.sqrt(dataValues["displacement"]/(dataValues["stroke"]*Math.PI*dataValues["number_of_cylinders"]))*2.0;};
	computations["number_of_cylinders"] = function(dataValues){return dataValues["displacement"]/(Math.PI*Math.pow(dataValues["bore"]/2,2)*dataValues["stroke"]);};
	computations["displacement"] = function(dataValues){return Math.PI*Math.pow(dataValues["bore"]/2,2)*dataValues["stroke"]*dataValues["number_of_cylinders"];};

	additionalCalculations = new Array();
	additionalCalculations.push(function(){$("input[name$='cylinder_volume']").attr("value", $("input[name$='displacement']").attr("value")/$("input[name$='number_of_cylinders']").attr("value"));});
}




viewController["engine_compression"] = function() {
	computations = new Array();
	computations["compression_ratio"] = function(dataValues){
		var b = parseFloat(dataValues["bore"]);
		var hgt = parseFloat(dataValues["head_gasket_thickeness"]);
		var hgb = parseFloat(dataValues["head_gasket_bore"]);
		var inbd = parseFloat(dataValues["below_deck"]);
		var HV = parseFloat(dataValues["head_volume"]);
		var VV = parseFloat(dataValues["dome_volume"]);
		var s = parseFloat(dataValues["stroke"]);
		var GV = hgb*hgb*12.87*hgt;
		var DV = b*b*12.87*inbd;
		var PV = b*b*s*12.87;
		var NUM = (GV+DV+HV-VV+PV);
		var DENUM = (GV+DV+HV-VV);
		return (NUM/DENUM);			
	};
}


viewController["engine_milling"] = function() {
	computations = new Array();
	computations["amount_to_mill_heads"] = function(dataValues){
		var current = parseFloat(dataValues["current_cr"]);
		var desired = parseFloat(dataValues["desired_cr"]);
		var s = parseFloat(dataValues["stroke"]);			
		return s*((1.0/(current-1.0))-(1.0/(desired-1.0)));
	};	
}



viewController["g_force_from_sixty_feet"] = function() {
	computations = new Array();
	computations["sixty_foot"] = function(dataValues){	
		return 3.75/(Math.pow(parseFloat(dataValues["time"]),2))*32.0*parseFloat(dataValues["time"])*0.681818182;
	};
	computations["average_g_force"] = function(dataValues){	
		return 3.75/(Math.pow(parseFloat(dataValues["time"]),2));
	};	
}


viewController["gears"] = function() {
	computations = new Array();
	computations["speed"] = function(dataValues){return (dataValues["rpm"] * dataValues["tire_diameter"]) / (dataValues["rear_end_gear_ratio"] * 336.0);};
	computations["rpm"] = function(dataValues){return (dataValues["speed"] * dataValues["rear_end_gear_ratio"] * 336.0) / (dataValues["tire_diameter"]);};
	computations["tire_diameter"] = function(dataValues){return (dataValues["speed"] * dataValues["rear_end_gear_ratio"] * 336.0) / dataValues["rpm"];};
	computations["rear_end_gear_ratio"] = function(dataValues){return (dataValues["rpm"] * dataValues["tire_diameter"]) / (dataValues["speed"] * 336.0);};
}



viewController["head_chamber_volume"] = function() {
	computations = new Array();
	computations["head_chamber_volume"] = function(dataValues) {
		var CR = parseFloat(dataValues["desired_compression_ratio"]);
		var b = parseFloat(dataValues["bore"]);
		var hgb = parseFloat(dataValues["head_gasket_bore"]);
		var hgt = parseFloat(dataValues["head_gasket_thickness"]);
		var inbd = parseFloat(dataValues["deck_height"]);
		var VV = parseFloat(dataValues["dome_volume"]);
		var s = parseFloat(dataValues["stroke"]);
		var GV = hgb*hgb*12.87*hgt;
		var DV = b*b*12.87*inbd;
		var PV = b*b*s*12.87;
		return (CR*(GV+DV-VV)-GV+VV-DV-PV)/(1.0-CR);
	};
}




viewController["hp_from_boost"] = function() {
	computations = new Array();
	computations["new_horse_power"] = function(dataValues){
		return parseFloat(dataValues["hp"])*(14.7+parseFloat(dataValues["boost"]))/14.7;	
	};
}


viewController["hp_from_et_and_weight"] = function() {
	computations = new Array();
	computations["hp"] = function(dataValues){
		return dataValues["weight"] / Math.pow((dataValues["et"]/5.825),3.0);
	};
}



viewController["hp_from_injector_flow"] = function() {
	computations = new Array();
	computations["max_hp"] = function(dataValues){
		return (parseFloat(dataValues["injector_flow"])*Math.sqrt(parseFloat(dataValues["new_fuel_pressure"])/parseFloat(dataValues["injector_fuel_pressure"])))*(parseFloat(dataValues["number_of_cylinders"])*(parseFloat(dataValues["duty_cycle"])/100))/parseFloat(dataValues["bsfc"]);
	};
}


viewController["injector_pulse_width"] = function() {
	additionalCalculations = new Array();
	additionalCalculations.push(
	function(dataValues){	
			var d = parseFloat(dataValues["displacement"]);
			var i = parseFloat(dataValues["ideal_target_rpm"]);
			var p = parseFloat(dataValues["peak_rpm"]);
			var vE = parseFloat(dataValues["volumetric_efficiency_at_idle"])/100.0;
			var nI = parseFloat(dataValues["number_of_injectors"]);
			var b = parseFloat(dataValues["bsfc"]);
			var iHG = parseFloat(dataValues["idle_inhg"])*3.3864/100.0;
			var eHP = parseFloat(dataValues["expected_peak_hp"]);
			var iMin = parseFloat(dataValues["injector_min_open_time"]);
			var iS = parseFloat(dataValues["injector_size"]);
			var iPC = parseFloat(dataValues["injections_per_cycle"]);
			var eS = parseFloat(dataValues["engine_stroke"])/2.0;
			var cfm100 = 1.6;
			var cfm = (d * i / (1728.0 * eS)) * vE;
			var fuel = iS * nI;
			var lbm = cfm / cfm100 * b;
			var fuelms = (fuel / 60000.0);
			var pwm = ((lbm / (i * iPC / eS)) / fuelms);
			var correction = pwm * iHG;
			var pw = iMin + correction;
			var squirtsec = (p / 60.0 / eS * iPC);
			var maxpw = (1000.0 / squirtsec) * 0.85;
			populateFieldWithValueFormatted("injector_size_at_80",((eHP*b)/(.8*nI)));
			populateFieldWithValueFormatted("max_hp_at_85",((((eHP*b)/(.8*nI))*nI/b)*.85));
			populateFieldWithValueFormatted("max_pulse_width_at_85",maxpw);
			populateFieldWithValueFormatted("max_hp_yours_at_85",(iS*nI/b*0.85));
			populateFieldWithValueFormatted("idle_pulsewidth_installed_injectors",pw);
			populateFieldWithValueFormatted("projected_cc",(((((cfm*0.0807)/13.2)/6.263)*3785.4)/nI));
		}
	);
}



viewController["injector_size_change"] = function() {
	computations = new Array();
	computations["new_injector_size"] = function(dataValues){
		return parseFloat(dataValues["injector_size"])*Math.sqrt(parseFloat(dataValues["new_fuel_pressure"])/parseFloat(dataValues["fuel_pressure"]));
	};
	computations["new_injector_size_cc_min"] = function(dataValues){
		var nISlbs = parseFloat(dataValues["injector_size"])*Math.sqrt(parseFloat(dataValues["new_fuel_pressure"])/parseFloat(dataValues["fuel_pressure"]));
		return nISlbs/0.0951;
	};
}


viewController["injector_sizing"] = function() {
	computations = new Array();
	computations["lbs_hr"] = function(dataValues){
		return (dataValues["hp"]*dataValues["bsfc"])/(dataValues["number_of_cylinders"]*(dataValues["duty_cycle"]/100));
	};
	computations["cc_min"] = function(dataValues){
		var lHR = (dataValues["hp"]*dataValues["bsfc"])/(dataValues["number_of_cylinders"]*(dataValues["duty_cycle"]/100));
		return lHR/0.0951;
	};
}

viewController["intake_runner_length"] = function() {
	computations = new Array();
	computations["runner_length_street"] = function(dataValues){	
		var dur = parseFloat(dataValues["advertised_duration"]);
		var r = parseFloat(dataValues["rpm"]);
		var h = parseFloat(dataValues["runner_height"]);
		var w = parseFloat(dataValues["runner_width"]);
		var EVCDRACE = 720.0-(dur-30);
		var EVCDSTREET = 720.0-(dur-20);
		var D = Math.sqrt((h*w)/Math.PI)*2.0;
		return EVCDSTREET*650/(r*3)-(0.5*D);
	};
	computations["runner_length_race"] = function(dataValues){	
		var dur = parseFloat(dataValues["advertised_duration"]);
		var r = parseFloat(dataValues["rpm"]);
		var h = parseFloat(dataValues["runner_height"]);
		var w = parseFloat(dataValues["runner_width"]);
		var EVCDRACE = 720.0-(dur-30);
		var EVCDSTREET = 720.0-(dur-20);
		var D = Math.sqrt((h*w)/Math.PI)*2.0;
		return EVCDRACE*650/(r*3)-(0.5*D);
	};
	computations["valve_close_duration_street"] = function(dataValues){	
		var dur = parseFloat(dataValues["advertised_duration"]);
		var r = parseFloat(dataValues["rpm"]);
		var h = parseFloat(dataValues["runner_height"]);
		var w = parseFloat(dataValues["runner_width"]);
		var EVCDRACE = 720.0-(dur-30);
		var EVCDSTREET = 720.0-(dur-20);
		var D = Math.sqrt((h*w)/Math.PI)*2.0;
		return EVCDSTREET;
	};
	computations["valve_close_duration_race"] = function(dataValues){	
		var dur = parseFloat(dataValues["advertised_duration"]);
		var r = parseFloat(dataValues["rpm"]);
		var h = parseFloat(dataValues["runner_height"]);
		var w = parseFloat(dataValues["runner_width"]);
		var EVCDRACE = 720.0-(dur-30);
		var EVCDSTREET = 720.0-(dur-20);
		var D = Math.sqrt((h*w)/Math.PI)*2.0;
		return EVCDRACE;
	};
}

viewController["intake_runner_torque"] = function() {
	computations = new Array();
	computations["displacement"] = function(dataValues){
		var d = parseFloat(dataValues["displacement"]);
		var r = parseFloat(dataValues["rpm"]);
		var a = parseFloat(dataValues["intake_runner_area"]);
		var c = parseFloat(dataValues["number_of_cylinders"]);
		return ((a * 88200.0) / r) * c;	
	};
	computations["rpm"] = function(dataValues){
		var d = parseFloat(dataValues["displacement"]);
		var r = parseFloat(dataValues["rpm"]);
		var a = parseFloat(dataValues["intake_runner_area"]);
		var c = parseFloat(dataValues["number_of_cylinders"]);
		return (a * 88200.0) / (d / c);	
	};
	computations["intake_runner_area"] = function(dataValues){
		var d = parseFloat(dataValues["displacement"]);
		var r = parseFloat(dataValues["rpm"]);
		var a = parseFloat(dataValues["intake_runner_area"]);
		var c = parseFloat(dataValues["number_of_cylinders"]);
		return ((d / c) * r) / 88200.0;	
	};
	computations["number_of_cylinders"] = function(dataValues){
		var d = parseFloat(dataValues["displacement"]);
		var r = parseFloat(dataValues["rpm"]);
		var a = parseFloat(dataValues["intake_runner_area"]);
		var c = parseFloat(dataValues["number_of_cylinders"]);
		return Math.round((d/((a * 88200.0) / r)));	
	};
}



viewController["intercooler_efficiency"] = function() {
	computations = new Array();
	computations["efficiency"] = function(dataValues){
		return (((parseFloat(dataValues["inlet_air_temp"]) - parseFloat(dataValues["outlet_air_temp"]))/(parseFloat(dataValues["inlet_air_temp"]) - parseFloat(dataValues["air_temp"])))*100);
	};
}


viewController["lateral_g_speed"] = function() {
	computations = new Array();
	computations["lateral_g"] = function(dataValues){	
		return 1.225*parseFloat(dataValues["radius"])/(Math.pow(parseFloat(dataValues["time"]),2));
	};
	computations["speed"] = function(dataValues){
		var g = 1.225*parseFloat(dataValues["radius"])/(Math.pow(parseFloat(dataValues["time"]),2));
		return Math.sqrt(g*(15*parseFloat(dataValues["radius"])));
	};
}


viewController["lateral_weight_transfer"] = function() {
	computations = new Array();
	computations["lateral_weight_transfer"] = function(dataValues){	
		var w = parseFloat(dataValues["weight"]);
		var g = parseFloat(dataValues["g_force"]);
		var cg = parseFloat(dataValues["cg_height"]);
		var tW = parseFloat(dataValues["track_width"]);
		return (w*g*cg)/tW;
	};
}


viewController["leaf_spring_rate"] = function() {
	computations = new Array();
	computations["spring_rate"] = function(dataValues){	
		var W = parseFloat(dataValues["width_of_leaves"]);
		var N = parseFloat(dataValues["number_of_leaves"]);
		var T = parseFloat(dataValues["thickness_of_one_leaf"]);
		var L = parseFloat(dataValues["length_of_spring"]);
		return (W*N/12.0)*Math.pow((1000.0*T/L),3.0);
	};
}


viewController["mov"] = function() {
	additionalCalculations = new Array();
			additionalCalculations.push(
				function(dataValues){
					var ye = parseFloat(dataValues["your_end_time"]);
					var yd = parseFloat(dataValues["your_dial_in"]);
					var yt  = parseFloat(dataValues["your_reaction_time"]);
					var ym = parseFloat(dataValues["your_mph"]);
					var yp = ((ye-yd)+(yt-0.0)) *1000;
					var yr = Math.round(yp) / 1000;
					var ybo = Math.abs(ye - yd);
					
					if (yt < 0.0) {
						populateFieldWithValue("your_package", "RED LIGHT");
					} else if (ye < yd) {
						populateFieldWithValue("your_package", "BREAKOUT");
					} else {
						populateFieldWithValueFormatted("your_package", yr);
					}
					
					var te = parseFloat(dataValues["their_end_time"]);
					var td = parseFloat(dataValues["their_dial_in"]);
					var tt  = parseFloat(dataValues["their_reaction_time"]);
					var tm = parseFloat(dataValues["their_mph"]);
					var tp = ((te-td)+(tt-0.0)) *1000;
					var tr = Math.round(tp) / 1000;
					var tbo = Math.abs(te - td);
					
					if (tt < 0.0) {
						populateFieldWithValue("their_package", "RED LIGHT");
                    } else if (te < td) {
						populateFieldWithValue("their_package", "BREAKOUT");
                    } else {
						populateFieldWithValueFormatted("their_package", tr);
					}
					
					
					
					var x, z, w, y;
					var mfr = "";
                            if (tt < 0.0)
                                if (yt < 0.0)
                                    x = 999;         //double red light for x
                                else x = 888;
                                        
                                        if (ye > (yd-.001))
                                            z = 999;            //Your ET is greater than or equal to your Dial In for z
                                            
                                            if (te > (td-.001))
                                                w = 999;        //Their ET is greater than or equal to their Dial In for w
                                                
                                                if (ye < yd)
                                                    if (te < td)
                                                        y =999;      //double breakout for y
                                                    else y = 888;
                                                            
                                                            if (x == 999)
																populateFieldWithValue("winner", "You - Only Green Lights");
                                                                else if (yt < 0.0)
																	populateFieldWithValue("winner", "Them");
                                                                    else if (tt < 0.0)
																		populateFieldWithValue("winner", "You");
                                                                        else if (y == 999)
                                                                            if (ybo < tbo)
																				populateFieldWithValue("winner", "You");
                                                                                else if (tbo < ybo)
																					populateFieldWithValue("winner", "Them");
                                                                                    
                                                                                    if (x == 999)       //double red light
                                                                                        x = 999;            //acts as a place holder
                                                                                        else if (y == 999)    //double breakout
                                                                                            z = 000;      //changes value of z for next if statement
                                                                                            else if (z == 999)
                                                                                                if (yt > -0.001)
                                                                                                    if (yr < tr)
																										populateFieldWithValue("winner", "You");
                                                                                                        else if (w == 999)
                                                                                                            if (tt > -0.001)
                                                                                                                if (tr < yr)
																													populateFieldWithValue("winner", "Them");
                                                                                                                    
                                                                                                                    if (x == 999)
																														populateFieldWithValue("first", "Double Foul");
                                                                                                                        else if (yr < tr)
																															populateFieldWithValue("first", "You");
                                                                                                                            else if (tr < yr)
																																populateFieldWithValue("first", "Them");
                                                                                                                                
                                                                                                                                
                                                                                                                                
    var sec = yr-tr;
    if(sec<0)
        sec*=-1.0;
    var sect = sec* 1000;
    var secr = Math.round(sect) / 1000;
	populateFieldWithValueFormatted("mov", secr);
                                                                                                                                
    var ymf = ((ym * 1.466 * sec)*100);
    var ymfr0 = Math.round(ymf) / 100;       //rounded down to 1/100th   
//    float ymfr1 = round(ymf);     //rounded up to nearest whole foot
    var ymfr2 = Math.floor(ym * 1.466 *sec); //rounded down to nearest whole foot
    var ymfr3 = ymfr0 - ymfr2;           //determine decimal remainder 
    var ymfr4 = Math.floor(ymfr3 * 12);      //calculate remainder in inches
    var ymfr = ""+ymfr2+"ft - "+ymfr4+"in";
                                                                                                                                
    var tmf = ((tm * 1.466 * sec)*100);
    var tmfr0 = Math.round(tmf) / 100;       //rounded down to 1/100th   
//    float tmfr1 = round(tmf);         //rounded up to nearest whole foot
    var tmfr2 = Math.floor(tm * 1.466 *sec); //rounded down to nearest whole foot
    var tmfr3 = tmfr0 - tmfr2;           //determine decimal remainder   
    var tmfr4 = Math.floor(tmfr3 * 12);      //calculate remainder in inches
    var tmfr = ""+tmfr2+"ft - "+tmfr4+"in";
                                                                                                                                
                                                                                                                                if (yr > tr)
                                                                                                                                    mfr  = ymfr;
                                                                                                                                else mfr = tmfr;
                                                                                                                                        
    populateFieldWithValue("mov_ft", mfr);
					
					
					
					
				}
			);
}


viewController["mpg"] = function() {
	computations = new Array();
	computations["mpg"] = function(dataValues){	
		return parseFloat(dataValues["miles"])/parseFloat(dataValues["gallons"]);
	};
	computations["gallons"] = function(dataValues){	
		return parseFloat(dataValues["miles"])/parseFloat(dataValues["mpg"]);
	};
	computations["miles"] = function(dataValues){	
		return parseFloat(dataValues["mpg"])*parseFloat(dataValues["gallons"]);
	};
}


viewController["mustang_to_dynojet"] = function() {
	computations = new Array();
	computations["mustang"] = function(dataValues){	
		return parseFloat(dataValues["dynojet"])*0.92;
	};
	computations["dynojet"] = function(dataValues){	
		return parseFloat(dataValues["mustang"])/0.92;
	};
}


function computeAirFuelConversions() {
	var g;
	if(!isFieldEmpty("lambda"))
		g=getValueFromFieldByName("lambda")/0.0680272109;
	else if(!isFieldEmpty("gasoline"))
		g = getValueFromFieldByName("gasoline");
	else if(!isFieldEmpty("e85"))
		g=getValueFromFieldByName("e85")/0.664285714;
	else if(!isFieldEmpty("e100"))
		g=getValueFromFieldByName("e100")/0.61277551;
	else if(!isFieldEmpty("methanol"))
		g=getValueFromFieldByName("methanol")/0.43537415;
	else if(!isFieldEmpty("propane"))
		g=getValueFromFieldByName("propane")/1.05442177;
	else if(!isFieldEmpty("diesel"))
		g=getValueFromFieldByName("diesel")/0.986394558;
		
	populateFieldWithValueFormatted("lambda", g*0.0680272109);
	populateFieldWithValueFormatted("gasoline", g);
	populateFieldWithValueFormatted("e85", g*0.664285714);
	populateFieldWithValueFormatted("e100",  g*0.61277551);
	populateFieldWithValueFormatted("methanol", g*0.43537415);
	populateFieldWithValueFormatted("propane", g*1.05442177);
	populateFieldWithValueFormatted("diesel", g*0.986394558);
}




function computeNHRACorrFactors() {
	populateEmptyRequiredFieldWithValue("altitude", 0);
	populateEmptyRequiredFieldWithValue("et", 0);
	populateEmptyRequiredFieldWithValue("trap_speed", 0);
	var dataValues = getFilledRequiredFields();
	
	var alt=(dataValues["altitude"]/100);

	var etf = 1.003-(0.0013*alt);
	var tsf = 0.9961+(0.0014*alt);
	var et = dataValues["et"];
	var ts = dataValues["trap_speed"];
	if(et>0) {
		et*=etf;
	} else {
		et=etf;
	}
	if(ts>0) {
		ts*=tsf;
	} else {
		ts=tsf;
	}					
	populateFieldWithValueFormatted("et_factor", et);
	populateFieldWithValueFormatted("trap_speed_factor", ts);
}


viewController["nitrous_jet"] = function() {
	computations = new Array();
	computations["fuel_jet_size"] = function(dataValues){
		var fP = parseFloat(dataValues["fuel_pressure"]);
		var nP = parseFloat(dataValues["nitrous_pressure"]);
		var nJS = parseFloat(dataValues["nitrous_jet_size"]);
		var nJPS = parseFloat(dataValues["number_of_jets"]);
		var newHP = (nJS*254)*(nJS*2.54*nJPS)*70;
		var fJS = Math.pow(nJS*nJS/(5.8/3.4*9.649/Math.pow(nP/fP,0.5)*0.9),0.5);
		return fJS;
	};
	computations["hp"] = function(dataValues){
		var fP = parseFloat(dataValues["fuel_pressure"]);
		var nP = parseFloat(dataValues["nitrous_pressure"]);
		var nJS = parseFloat(dataValues["nitrous_jet_size"]);
		var nJPS = parseFloat(dataValues["number_of_jets"]);
		var newHP = (nJS*254)*(nJS*2.54*nJPS)*70;
		var fJS = Math.pow(nJS*nJS/(5.8/3.4*9.649/Math.pow(nP/fP,0.5)*0.9),0.5);
		return newHP;			
	};
}


viewController["octane_calculator"] = function() {
	computations = new Array();
	computations["amount_of_lower"] = function(dataValues){
		var dO = parseFloat(dataValues["desired_octane"]);
		var lO = parseFloat(dataValues["lower_octane"]);
		var hO = parseFloat(dataValues["higher_octane"]);
		var dG = parseFloat(dataValues["desired_gallons"]);
		return (dG - (dG * ((dO - lO) / (hO - lO)))); 
	};
	computations["amount_of_higher"] = function(dataValues){
		var dO = parseFloat(dataValues["desired_octane"]);
		var lO = parseFloat(dataValues["lower_octane"]);
		var hO = parseFloat(dataValues["higher_octane"]);
		var dG = parseFloat(dataValues["desired_gallons"]);
		return (dG * ((dO - lO) / (hO - lO))); 
	};
}


viewController["optimum_header_length"] = function() {
	computations = new Array();
	computations["optimum_length"] = function(dataValues){
		var d = parseFloat(dataValues["displacement"]);
		var r = parseFloat(dataValues["rpm"]);
		var o = parseFloat(dataValues["outside_diameter_header"]);
		return ((d * 1900.0) / (r * (o*o))); 	
	};
	computations["area_of_primary_pipe"] = function(dataValues){
		var d = parseFloat(dataValues["displacement"]);
		var r = parseFloat(dataValues["rpm"]);
		var o = parseFloat(dataValues["outside_diameter_header"]);
		return r*d/705600.0-0.049; 	
	};
	additionalCalculations = new Array();
	additionalCalculations.push(
		function(dataValues){
			var d = parseFloat(dataValues["displacement"]);
			var r = parseFloat(dataValues["rpm"]);
			var o = parseFloat(dataValues["outside_diameter_header"]);
			populateFieldWithValue("inside_diameter", ""+(Math.sqrt(r*d/554177.0)).toFixed(3)+", "+(Math.sqrt(r*d/554177.0)*.9).toFixed(3)); 	
		}
	);
}

viewController["overboring"] = function() {
	computations = new Array();
	computations["new_displacement"] = function(dataValues){
		var b = parseFloat(dataValues["bore"]);
		var o = parseFloat(dataValues["overbore"]);
		var s = parseFloat(dataValues["stroke"]);
		var c = parseFloat(dataValues["number_of_cylinders"]);
		return (b+o)*(b+o)/4.0*s*Math.PI*c;			
	};
}


viewController["piston_speed_accel"] = function() {
	computations = new Array();
	computations["fpm"] = function(dataValues){return dataValues["stroke"]*dataValues["rpm"]/6.0;};
	computations["max_piston_acceleration"] = function(dataValues){return ((dataValues["rpm"]*dataValues["rpm"]*dataValues["stroke"])/2189.0)*(1.0+1.0/(2*dataValues["connecting_rod_length"]/dataValues["stroke"]));};
}


viewController["pitot_press_to_fps"] = function() {
	computations = new Array();
	computations["pitot"] = function(dataValues){
		return Math.pow((parseFloat(dataValues["fps"])/66.2),2);	
	};
	computations["fps"] = function(dataValues){
		return Math.sqrt(parseFloat(dataValues["pitot"]))*66.2;	
	};
}

viewController["power_to_weight_ratio"] = function() {
	computations = new Array();
	computations["power_to_weight_ratio"] = function(dataValues) {
		return dataValues["power"]/dataValues["weight"];
	};
}

viewController["quarter_et_mph_from_hp_weight"] = function() {
	computations = new Array();
	computations["et"] = function(dataValues){
		return Math.pow((dataValues["weight"]/dataValues["hp"]),0.333333)*5.825;
	};
	computations["mph"] = function(dataValues){
		return Math.pow((dataValues["hp"]/dataValues["weight"]),0.333333)*234.0;
	};
}

viewController["rpm_drop_after_shift"] = function() {
	computations = new Array();
	computations["transmission_ratio_from"] = function(dataValues){	
		var tRF = parseFloat(dataValues["transmission_ratio_from"]);
		var tRT = parseFloat(dataValues["transmission_ratio_into"]);
		var rBS = parseFloat(dataValues["rpm_before_shift"]);
		var rAS = parseFloat(dataValues["rpm_after_shift"]);
		return tRT/(rAS/rBS);
	};
	computations["transmission_ratio_into"] = function(dataValues){	
		var tRF = parseFloat(dataValues["transmission_ratio_from"]);
		var tRT = parseFloat(dataValues["transmission_ratio_into"]);
		var rBS = parseFloat(dataValues["rpm_before_shift"]);
		var rAS = parseFloat(dataValues["rpm_after_shift"]);
		return rAS*(tRF/rBS);
	};
	computations["rpm_before_shift"] = function(dataValues){	
		var tRF = parseFloat(dataValues["transmission_ratio_from"]);
		var tRT = parseFloat(dataValues["transmission_ratio_into"]);
		var rBS = parseFloat(dataValues["rpm_before_shift"]);
		var rAS = parseFloat(dataValues["rpm_after_shift"]);
		return tRF/(tRT/rAS);
	};
	computations["rpm_after_shift"] = function(dataValues){	
		var tRF = parseFloat(dataValues["transmission_ratio_from"]);
		var tRT = parseFloat(dataValues["transmission_ratio_into"]);
		var rBS = parseFloat(dataValues["rpm_before_shift"]);
		var rAS = parseFloat(dataValues["rpm_after_shift"]);
		return tRT/(tRF/rBS);
	};
}

function reset_all() {
	reset();
	clearTable();
}

function clearTable() {
	$("#speed_table tbody").html("");
}


function computeSpeedTransmission() {
	var dataValues = getFilledRequiredFields();
	clearTable();
	
	var firstGearRatio = parseFloat(dataValues["first"]);
	var secondGearRatio = parseFloat(dataValues["second"]);
	var thirdGearRatio = parseFloat(dataValues["third"]);
	var fourthGearRatio = parseFloat(dataValues["fourth"]);
	var fifthGearRatio = parseFloat(dataValues["fifth"]);
	var sixthGearRatio = parseFloat(dataValues["sixth"]);
	var rearEndRatio = parseFloat(dataValues["gear_ratio"]);
	var tireDiam = parseFloat(dataValues["tire_diameter"]);
	var torqueConverterSlip = dataValues["torque_converter_slip"]?parseFloat(dataValues["torque_converter_slip"]):0;
	
	for(var i = 100; i < 15000; i+=100) {
		var one = (i * tireDiam) / (firstGearRatio *rearEndRatio* 336.0) * (1-torqueConverterSlip);
		var two = (i * tireDiam) / (secondGearRatio *rearEndRatio* 336.0) * (1-torqueConverterSlip);
		var three = (i * tireDiam) / (thirdGearRatio *rearEndRatio* 336.0) * (1-torqueConverterSlip);
		var four = (i * tireDiam) / (fourthGearRatio *rearEndRatio* 336.0) * (1-torqueConverterSlip);
		var five = (i * tireDiam) / (fifthGearRatio *rearEndRatio* 336.0) * (1-torqueConverterSlip);
		var six = (i * tireDiam) / (sixthGearRatio *rearEndRatio* 336.0) * (1-torqueConverterSlip);
		one = one.toFixed(1);
		two = two.toFixed(1);
		three = three.toFixed(1);
		four = four.toFixed(1);
		five = five.toFixed(1);
		six = six.toFixed(1);
		$("#speed_table tbody").append("<tr><td>"+i+"</td><td>"+one+"</td><td>"+two+"</td><td>"+three+"</td><td>"+four+"</td><td>"+five+"</td><td>"+six+"</td></tr>");
	}
}

viewController["speedometer_error"] = function() {
	computations = new Array();
	computations["correction_factor"] = function(dataValues){	
		return Math.abs(parseFloat(dataValues["actual_speed"])-parseFloat(dataValues["indicated_speed"]))/parseFloat(dataValues["actual_speed"])*100.0; 
	};
}



viewController["spring_rate"] = function() {
	computations = new Array();
	computations["spring_rate"] = function(dataValues){	
		var d = parseFloat(dataValues["wire_diameter"]);
		var N = parseFloat(dataValues["active_coils"]);
		var D = parseFloat(dataValues["mean_coil_diameter"]);
		return (11250000.0*d*d*d*d)/(8*N*D*D*D);
	};
}



viewController["tb_runner_diameter"] = function() {
	computations = new Array();
	computations["tb_runner_diameter"] = function(dataValues){	
		var d = parseFloat(dataValues["displacement"]);
		var r = parseFloat(dataValues["rpm"]);
		var v = parseFloat(dataValues["volumetric_efficiency"]);
		return Math.sqrt(d*v/100.0*r/203400.0);
	};
}



viewController["timing_mark_calculator"] = function() {
	computations = new Array();
	computations["from"] = function(dataValues){	
		return parseFloat(dataValues["diameter"])*Math.PI/360.0*parseFloat(dataValues["degree"]);
	};
}



viewController["tire_diameter"] = function() {
	computations = new Array();
	computations["tire_width"] = function(dataValues){return (25.4/2.0)*(dataValues["tire_diameter"]-dataValues["wheel_diameter"])/(dataValues["aspect_ratio"]*.01);};
	computations["aspect_ratio"] = function(dataValues){return ((25.4/2.0)*(dataValues["tire_diameter"]-dataValues["wheel_diameter"])/dataValues["tire_width"])*100;};
	computations["wheel_diameter"] = function(dataValues){return  dataValues["tire_diameter"]-((dataValues["tire_width"]*dataValues["aspect_ratio"]*.01/25.4)*2);};
	computations["tire_diameter"] = function(dataValues){return ((dataValues["tire_width"]*dataValues["aspect_ratio"]*.01/25.4)*2)+parseFloat(dataValues["wheel_diameter"]);};
}



viewController["valve_lift_rocker_ratio"] = function() {
	computations = new Array();
	computations["new_lift_after_change"] = function(dataValues){	
		var crar = parseFloat(dataValues["current_rocker_arm_ratio"]);
		var vA = parseFloat(dataValues["valve_lift_of_lobe_affected"]);
		var nrar = parseFloat(dataValues["new_rocker_arm_ratio"]);
		return nrar/crar*vA; 
	};
}



viewController["volumetric_efficiency"] = function() {
	computations = new Array();
	computations["volumetric_efficiency"] = function(dataValues){return (3456.0*dataValues["cfm"])/(dataValues["displacement"]*dataValues["rpm"])*100.0;};
	computations["cfm"] = function(dataValues){return dataValues["displacement"]*dataValues["rpm"]*dataValues["volumetric_efficiency"]*0.01/3456;};
	computations["displacement"] = function(dataValues){return (3456*dataValues["cfm"])/(dataValues["rpm"]*dataValues["volumetric_efficiency"]*0.01);};
	computations["rpm"] = function(dataValues){return (3456*dataValues["cfm"])/(dataValues["volumetric_efficiency"]*0.01*dataValues["displacement"]);};
}



viewController["wheel_rate"] = function() {
	computations = new Array();
	computations["wheel_rate"] = function(dataValues){	
		var a = parseFloat(dataValues["A"]);
		var b = parseFloat(dataValues["B"]);
		var c = parseFloat(dataValues["C"]);
		var d = parseFloat(dataValues["D"]);
		var sR = parseFloat(dataValues["spring_rate"]);
		return sR*Math.pow((a/b),2.0)*Math.pow((c/d),2.0);;
	};
}



viewController["wind_resistance"] = function() {
	computations = new Array();
	computations["drag_force"] = function(dataValues){
		var v = dataValues["speed"];
		var A = dataValues["frontal_area"];
		var Cd = dataValues["drag_coefficient"]
		v*=.44704; //convert mph to m/s
		A*=0.09290304; //convert from ft^2 to m^2
		var Fd = Cd*0.5*1.225*A*v*v; //answer in N
		var Pd = Fd*v; //in W
		Fd*=0.224808943; //convert N to lbs force
		Pd*=0.00134102209;
		return Fd;
	};
	computations["power_required"] = function(dataValues){
		var v = dataValues["speed"];
		var A = dataValues["frontal_area"];
		var Cd = dataValues["drag_coefficient"]
		v*=.44704; //convert mph to m/s
		A*=0.09290304; //convert from ft^2 to m^2
		var Fd = Cd*0.5*1.225*A*v*v; //answer in N
		var Pd = Fd*v; //in W
		Fd*=0.224808943; //convert N to lbs force
		Pd*=0.00134102209;
		return Pd;
	};
}



function computeCamTimingEvents() {
	var dataValues = getFilledRequiredFields();
	var iD = parseFloat(dataValues["intake_duration"]);
	var eD = parseFloat(dataValues["exhaust_duration"]);
	var iCent = parseFloat(dataValues["intake_centerline"]);
	var lsa = parseFloat(dataValues["lobe_separation"]);
	var cA = parseFloat(dataValues["cam_advance"]);
	var iO = (iD/2)-iCent+cA;
	var iC = iD-180-iO;
	var eCent = lsa*2-iCent;
	var eC = (eD/2)-eCent-cA;
	var eO = eD-180-eC;
	var o = iO+eC;
	if(iO>=0) {
		populateEmptyRequiredFieldWithValue("intake_open", "" + iO + "� BTDC");
	} else {
		iO*=-1.0;
		populateEmptyRequiredFieldWithValue("intake_open", "" + iO + "� ATDC");
	}
	
	populateEmptyRequiredFieldWithValue("exhaust_open", "" + eO + "� BBDC");
	populateEmptyRequiredFieldWithValue("intake_close", "" +iC + "� ABDC");
	
	if(eC>0) {
		populateEmptyRequiredFieldWithValue("exhaust_close", "" + eC + "� ATDC");
	} else {
		eC*=-1.0;
		populateEmptyRequiredFieldWithValue("exhaust_close", "" + eC + "� BTDC");
	}
	
	populateEmptyRequiredFieldWithValue("overlap", "" + o + "�");
}