var additionalCalculations;
var computations;

function reset() {
	$(".required").each(function(i, d){
		d.value="";
	});
	$(".to_find").each(function(i, d){
		d.value="";
	});
	$(".optional").each(function(i, d){
		d.value="";
	});
}

function getValueFromFieldByName(field) {
	return $("input[name$='"+field+"']").attr("value");
}

function populateEmptyRequiredFieldWithValue(field, value) {
	if($("input[name$='"+field+"']").attr("value")=="") {
		$("input[name$='"+field+"']").attr("value", value);
	}
}

function populateFieldWithValue(field, value) {
	$("input[name$='"+field+"']").attr("value", value);
}

function populateFieldWithValueFormatted(field, value) {
	$("input[name$='"+field+"']").attr("value", value.toFixed(4));
}

function calculate(allButOne, additionalCalcs) {
	var dataValues = getFilledRequiredFields();
	if(allButOne) {
		if($(".required").length-1 == countCompletedFields()) {
			var formulaName = getEmptyFieldName();
			var returnValue = computations[formulaName](dataValues);
			$("input[name$='"+formulaName+"']").attr("value", returnValue.toFixed(4));
			
			if(additionalCalcs) {
				if(additionalCalculations.length>0) {
					for(funct in additionalCalculations) {
						additionalCalculations[funct](dataValues);
					}
				}
			}
		} else {
			alert("Please fill in all but one field");
		}
	} else {
		if($(".required").length == countCompletedFields()) {
			$(".to_find").each(function(i, val){
				val.value = computations[val.name](dataValues).toFixed(4);
			});
			if(additionalCalcs) {
				if(additionalCalculations.length>0) {
					for(funct in additionalCalculations) {
						additionalCalculations[funct](dataValues);
					}
				}
			}
		} else {
			alert("Please complete all required fields!");
		}
	}
}

function getEmptyFieldName() {
	var fieldName = "";
	$(".required").each(function(i, d){
		if(d.value=="") {
			fieldName = d.name;
		}
	});
	return fieldName;
}

function getFilledRequiredFields() {
	var dataValues = new Array();
	$(".required").each(function(i, d){
		if(d.value!="") {
			dataValues[d.name] = d.value;
		}
	});
	return dataValues;
}

function countCompletedFields() {
	var count = 0;
	$(".required").each(function(i, d){
		if(d.value!="") {
			count++;
		}
	});
	return count;
}

function isFieldEmpty(field) {
	return $("input[name$='"+field+"']").attr("value")=="";
}