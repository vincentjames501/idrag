function showMap(position) {
	var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    getZipCode(latLng);  
}

function autoCompute() {
	var tc = parseFloat(getValueFromFieldByName("air_temperature"));		// temperature, deg C
	var tdpc = parseFloat(getValueFromFieldByName("dew_point"));			// dewpoint, deg C
	var altsetmb = parseFloat(getValueFromFieldByName("altimeter"));		// altimeter setting, mb
	var zm = parseFloat(getValueFromFieldByName("altitude"));			// geometric elevation, m
	tc = (5.0/9.0)*(tc-32.0);
	tdpc = (5.0/9.0)*(tdpc-32.0);
	altsetmb*=33.8637526;
	zm*=0.3048;
	var emb = calcVaporPressure_wobus(tdpc);
	var hm = calcH(zm);
	var actpressmb = calcAs2Press(altsetmb, hm);
	var density = calcDensity(actpressmb, emb, tc);
	var relden=100*(density/1.225);
	var densaltm = calcAltitude(density);
	var densaltzm = calcZ(densaltm);
	if ( densaltzm > 11000 || densaltzm < -5000 ) {
		alert("Out of range for Troposhere Algorithm");
		return;
	}
	densaltzm *= 3.2808399;
	actpressmb *= 0.0295301;
	populateFieldWithValueFormatted("da", parseFloat(densaltzm));
	populateFieldWithValueFormatted("absolute_pressure", parseFloat(actpressmb));
	populateFieldWithValueFormatted("relative_density", parseFloat(relden));
	//$.mobile.hidePageLoadingMsg();	 
}

function calcZ(h) {
	var r=6369E3;
	return ((r*h)/(r-h));
}

function calcAltitude(d) {
	var g=9.80665;
	var Po=101325;
	var To=288.15;
	var L=6.5;
	var R=8.314320;
	var M=28.9644;
	
	var D=d*1000;
	
	var p2=( (L*R)/(g*M-L*R) )*Math.log( (R*To*D)/(M*Po) );
	
	var H=-(To/L)*( Math.exp(p2)-1 );
	
	var h=H*1000;
	
	return(h);
}

function calcDensity(abspressmb, e, tc) {
	var Rv=461.4964;
	var Rd=287.0531;
	
	var tk=tc+273.15;
	var pv=e*100;
	var pd= (abspressmb-e)*100;
	var d= (pv/(Rv*tk)) + (pd/(Rd*tk));
	return(d);
}

function calcAs2Press(As, h) {
	var k1=.190263;
	var k2=8.417286E-5;
	
	var p=Math.pow((Math.pow(As,k1)-(k2*h)),(1/k1) );
	
	return (p);
}

function calcH(z) {
	var r=6369E3;
	return ((r*z)/(r+z));
}

function calcVaporPressure_wobus(t) {
	var eso=6.1078;
	
	var c0=0.99999683;
	var c1=-0.90826951E-02;
	var c2=0.78736169E-04;
	var c3=-0.61117958E-06;
	var c4=0.43884187E-08;
	var c5=-0.29883885E-10;
	var c6=0.21874425E-12;
	var c7=-0.17892321E-14;
	var c8=0.11112018E-16;
	var c9=-0.30994571E-19;
	
	var pol=c0+t*(c1+t*(c2+t*(c3+t*(c4+t*(c5+t*(c6+t*(c7+t*(c8+t*(c9)))))))));
	
	var es=eso/Math.pow(pol,8);
	
	return (es);
}

function getTemperatureData(latLng, zip_code) {
	YUI().use('yql', function (Y) {
              Y.YQL('select * from weather.forecast where location =' + zip_code, function (r) {
                    populateFieldWithValueFormatted("air_temperature", parseFloat(r.query.results.channel.item.condition.temp));
                    populateFieldWithValueFormatted("altimeter", parseFloat(r.query.results.channel.atmosphere.pressure));
                    populateFieldWithValueFormatted("dew_point", computeDewPoint(r.query.results.channel.atmosphere.humidity, r.query.results.channel.item.condition.temp));
                    getElevation(latLng);
                    });
              
              });
}

function computeDewPoint(RH, tempF) {
    var tempC = (5.0/9.0)*(parseFloat(tempF)-32);
    var alpha = 17.271;
    var beta = 237.7;
    var gamma = (alpha*tempC)/(beta+tempC)+Math.log(parseFloat(RH)/100.0);
    var dewPointC = (beta*gamma)/(alpha-gamma);
    return dewPointC*(9.0/5.0)+32.0;
}

function getZipCode(latLng) {
	var geocoder = new google.maps.Geocoder();
	if (geocoder) {
		geocoder.geocode({
                         'latLng': latLng
                         }, function (results, status) {
                         for (addressIndex in results[0].address_components) {
                         var address = results[0].address_components[addressIndex];
                         if ($.inArray("postal_code", address.types) != -1) {
                         populateEmptyRequiredFieldWithValue("zip", address.long_name);
                         getTemperatureData(latLng, address.long_name);
                         }
                         }
                         });
	}
}

function getElevation(latLng) {
	var locations = new Array();
    locations.push(latLng);
    var elevator = new google.maps.ElevationService();
    var positionalRequest = {
        'locations': locations
    }
    
    elevator.getElevationForLocations(positionalRequest, function (results, status) {
                                      populateFieldWithValueFormatted("altitude", results[0].elevation * 3.2808399);
                                      autoCompute();
                                      });
}

function errorCallback(error) {
	//$.mobile.hidePageLoadingMsg();
    alert("Your device does not support GPS lookup!");
}

function autoButtonPressed() {
    //$.mobile.showPageLoadingMsg();	
	navigator.geolocation.getCurrentPosition(showMap, errorCallback); 
}